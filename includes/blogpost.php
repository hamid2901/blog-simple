<?php

class BlogPost
{

public $id;
public $title;
public $post;
public $author;
public $tags;
public $datePosted;

function __construct ($inId=null, $inTitle=null, $inPost=null, $inPostFull=null, $inAuthorId=null, $inDatePosted=null)
{
if(!empty ($inId))
{
$this->id = $inId;
}
if (!empty($inTitle))
{
$this->title = $inTitle;
}
if (!empty($inPost))
{
$this->post = $inPost;
}
if (!empty($inDatePosted))
{
$splitDate = explode ("-", $inDatePosted);
$this->datePosted = $splitDate[1] . "/" . $splitDate[2] . "/" . $splitDate[0];
}
try {
	$con = new PDO("mysql:host=localhost;dbname=blog-simple", 'root', '');
	// set the PDO error mode to exception
	$con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
   
	echo "Connected successfully";
}
catch(PDOException $e)
	{
	echo "Connection Failed" . $e->getMessage();
	}
if (!empty($inAuthorId))
{
    $sql = $con->prepare("SELECT first_name, last_name FROM people WHERE id = " . $inAuthorId);
    // use exec() because no results are returned
    $sql->execute();
    $row = $sql->fetch();

// $query = mysql_query("SELECT first_name, last_name FROM people WHERE id = " . $inAuthorId);
// $row = mysql_fetch_assoc ($query);
$this->author = $row["first_name"] . " " . $row["last_name"];
}
$postTags = "No Tags";
if (!empty($inId))
{
    $sql = $con->prepare("SELECT tags.* FROM blog_post_tags LEFT JOIN (tags) ON (blog_post_tags.tag_id = tags.id) WHERE blog_post_tags.blog_post_id = " . $inId);
    // use exec() because no results are returned
    $sql->execute();

// $query = mysql_query("SELECT tags.* FROM blog_post_tags LEFT JOIN (tags) ON (blog_post_tags.tag_id = tags.id) WHERE blog_post_tags.blog_post_id = " . $inId);
$tagArray = array();
$tagIDArray = array();
while ($row = $sql->fetch())
{
$tagArray[]=$row["name"];
$tagIDArray[]=$row["id"];
}
if (sizeof($tagArray) > 0)
{
foreach ($tagArray as $tag)
{
$tagArray[]=$row["name"];
$tagIDArray[]=$row["id"];
}
if (sizeof($tagArray) > 0)
{
foreach ($tagArray as $tag)
{
if ($postTags == "No Tags")
{
$postTags = $tag;
}
else
{
$postTags = $postTags . ", " . $tag;
}
}
}
}
$this->tags = $postTags;
}
}
}
?>
