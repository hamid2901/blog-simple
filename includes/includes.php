<?php
include 'blogpost.php';

try {
	$con = new PDO("mysql:host=localhost;dbname=blog-simple", 'root', '');
	// set the PDO error mode to exception
	$con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
   
	echo "Connected successfully";
}
catch(PDOException $e)
	{
	echo "Connection Failed" . $e->getMessage();
	}

// // Change this info so that it works with your system.
// $connection = mysql_connect('localhost', 'root', '') or die ("<p class='error'>Sorry, we were unable to connect to the database server.</p>");
// $database = "blog";
// mysql_select_db($database, $connection) or die ("<p class='error'>Sorry, we were unable to connect to the database.</p>");

function GetBlogPosts($inId=null, $inTagId =null)
{
	$con = $GLOBALS['con'];
	if (!empty($inId))
	{
		$sql = $con->prepare("SELECT * FROM blog_posts WHERE id = " . $inId . " ORDER BY id DESC");
		// use exec() because no results are returned
		$sql->execute();

		// $query = mysql_query("SELECT * FROM blog_posts WHERE id = " . $inId . " ORDER BY id DESC"); 
	}
	else if (!empty($inTagId))
	{
		$sql = $con->prepare("SELECT blog_posts.* FROM blog_post_tags LEFT JOIN (blog_posts) ON (blog_post_tags.blog_post_id = blog_posts.id) WHERE blog_post_tags.tag_id =" . $inTagId . " ORDER BY blog_posts.id DESC");
		// use exec() because no results are returned
		$sql->execute();
		
		// $query = mysql_query("SELECT blog_posts.* FROM blog_post_tags LEFT JOIN (blog_posts) ON (blog_post_tags.blog_post_id = blog_posts.id) WHERE blog_post_tags.tag_id =" . $inTagId . " ORDER BY blog_posts.id DESC");
	}
	else
	{
		$sql = $con->prepare("SELECT * FROM blog_posts ORDER BY id DESC");
		// use exec() because no results are returned
		$sql->execute();
		// $con->exec($sql);
		
		// $query = mysql_query("SELECT * FROM blog_posts ORDER BY id DESC");
	}
	if ($sql == false) {
		return false;
	} 
	$postArray = array();
	while ($row = $sql->fetch())
	{
		$myPost = new BlogPost($row['id'], $row['title'], $row['post'], $row['post'], $row["author_id"], $row["date_posted"]);
		$postArray[]= $myPost;
	}
	return $postArray;
}
?>